#ifndef DISPLAY_H
#define	DISPLAY_H

#include <xc.h> 
#include "bus_058.h"

#define DIG1 PORTDbits.RD0
#define DIG1_TRIS TRISD0

#define DIG2 PORTDbits.RD1
#define DIG2_TRIS TRISD1

#define DIG3 PORTDbits.RD2
#define DIG3_TRIS TRISD2

#define DIG4 PORTDbits.RD3
#define DIG4_TRIS TRISD3

#define SH_CP_DIG PORTCbits.RC7
#define SH_CP_DIG_TRIS TRISC7

#define DS_DIG PORTCbits.RC5
#define DS_DIG_TRIS TRISC5

#define ST_CP_DIG PORTCbits.RC6
#define ST_CP_DIG_TRIS TRISC6

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    const uint8_t _OFF = 0xFF; //All segments disabled 
    const uint8_t _ON = 0; //All segments disabled 
    
    //Digits
    const uint8_t _ZERO = 0b00000011;
    const uint8_t _ONE = 0b10011111;
    const uint8_t _TWO = 0b00100101;
    const uint8_t _THREE = 0b00001101;
    const uint8_t _FOUR = 0b10011001;
    const uint8_t _FIVE = 0b01001001;
    const uint8_t _SIX = 0b01000001;
    const uint8_t _SEVEN = 0b00011111;
    const uint8_t _EIGHT = 0b00000001;
    const uint8_t _NINE = 0b00001001;
    
    //Letters
    const uint8_t _A = 0b00010001;
    const uint8_t _B = 0b11000001;
    const uint8_t _C = 0b01100011;
    const uint8_t _D = 0b10000001;
    const uint8_t _E = 0b01100001;
    const uint8_t _F = 0b01110001;
    
    const uint8_t _I = 0b10011111;
    const uint8_t _L = 0b11100011;

    const uint8_t _U = 0b10000011;
    const uint8_t _S = 0b01001001;
    const uint8_t _Y = 0b10001001;
    
    const uint8_t _P = 0b00110001;
    const uint8_t _R = 0b11110101;
    
    const uint8_t _JUMP = 0b00010011;
    const uint8_t _LOOP = 0b10000010;
    
    void init_display();
    
    void set_display_symbol( uint8_t symbol, uint8_t digit );
    
    void toggle_digit_point( bool state, uint8_t digit );
    
    void set_all_display_symbols( uint8_t symbol1, uint8_t symbol2, uint8_t symbol3, uint8_t symbol4 );
    
    void show_display();
       
    void commit_display();
    
    void shift_display_left( uint8_t n );
    
    uint8_t get_display_symbol( uint8_t digit );
    
    uint8_t digit_to_symbol( uint8_t digit );
    
#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

