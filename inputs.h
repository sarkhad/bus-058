
#ifndef INPUTS_H
#define	INPUTS_H

#include <xc.h>
#include "bus_058.h"

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

#define INPUT1 PORTAbits.RA5
#define INPUT1_TRIS TRISA5

#define INPUT2 PORTAbits.RA4
#define INPUT2_TRIS TRISA4
    
#define INPUT3 PORTAbits.RA3
#define INPUT3_TRIS TRISA3

    void init_inputs(void);
    
    uint8_t get_jump_location( uint8_t mask );
    
#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

