
#ifndef TESTER_H
#define	TESTER_H

#include "bus_058.h"
#include <xc.h> 

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    void check_system();
    
#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

