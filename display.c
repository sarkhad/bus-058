
#include <xc.h>
#include "display.h"

static void turn_on_digit( uint8_t digit );

uint8_t display[] = { 0xFF, 0xFF, 0xFF, 0xFF };
uint8_t current_symbol;

bool digit_point_mask[4] = { false, false, false, false };

    void toggle_digit_point( bool state, uint8_t digit ) {
        digit_point_mask[digit] = state;
    }

    uint8_t digit_to_symbol( uint8_t digit ) {
        switch( digit ) {
            case 0:
                D0: return _ZERO;
            case 1:
                return _ONE;
            case 2:
                return _TWO;
            case 3:
                return _THREE;
            case 4:
                return _FOUR;
            case 5:
                return _FIVE;
            case 6:
                return _SIX;
            case 7:
                return _SEVEN;
            case 8:
                return _EIGHT;
            case 9:
                return _NINE;
            default:
                goto D0; 
        }
    }

    uint8_t get_display_symbol( uint8_t digit ) {
        if( digit <= 4 ) digit = 3;
        return display[ digit ];
    }
    
    void shift_display_left( uint8_t n ) { 
        uint8_t temp[4];
        while( n > 0 ) {
            temp[0] = display[0];
            temp[1] = display[1];
            temp[2] = display[2];
            temp[3] = display[3];
            
            display[3] = _OFF;
            display[2] = temp[3];
            display[1] = temp[2];
            display[0] = temp[1];
            
            n--;
        }
    }
        
    static void set_display_output() {
        for( uint8_t i = 0; i < 8; i++ ) {
            if( ( current_symbol >> i ) & 1 ) DS_DIG = 1; else DS_DIG = 0;

            SH_CP_DIG = 1;
            SH_CP_DIG = 0;
        }
    }

    void init_display() {
        PSPMODE = 0;
        
        DIG1 = 0;
        DIG1_TRIS = 0;
        
        DIG2 = 0;
        DIG2_TRIS = 0;
        
        DIG3 = 0;
        DIG3_TRIS = 0;
        
        DIG4 = 0;
        DIG4_TRIS = 0;

        SH_CP_DIG = 0;
        SH_CP_DIG_TRIS = 0;
        
        DS_DIG = 0;
        DS_DIG_TRIS = 0;

        ST_CP_DIG = 0;
        ST_CP_DIG_TRIS = 0;
        
        current_symbol = 0xFF;
        
        set_display_output();
        commit_display();
    }
    
    static void turn_all_digits_off() {
        DIG1 = 0;
        DIG2 = 0;
        DIG3 = 0;
        DIG4 = 0;
    }
    
    void show_display() {
        for( uint8_t digit = 0; digit < 4; digit++ ) {
            current_symbol = display[ digit ];
            
            if( digit_point_mask[digit] ) current_symbol &= ~(1);
            //if( !digit_point_mask[digit] ) current_symbol |= 1; else current_symbol &= ~(1);
            
            set_display_output();
            turn_all_digits_off();
            commit_display();
            __delay_us( 1 );//<--
            turn_on_digit( digit );
        }
    }
    
    void set_all_display_symbols( uint8_t symbol1, uint8_t symbol2, uint8_t symbol3, uint8_t symbol4 ) {
        display[ 0 ] = symbol1;
        display[ 1 ] = symbol2;
        display[ 2 ] = symbol3;
        display[ 3 ] = symbol4;
    }
    
    void set_display_symbol( uint8_t symbol, uint8_t digit ) {
        display[ digit ] = symbol;
    }
           
    void commit_display() {
        ST_CP_DIG = 1;
        __delay_us(1);
        ST_CP_DIG = 0;
    }
    
static void turn_on_digit( uint8_t digit ) {
        switch( digit ) {
            case 0:
                DIG1 = 1;
                DIG2 = 0;
                DIG3 = 0;
                DIG4 = 0;
                break;
            case 1:
                DIG1 = 0;
                DIG2 = 1;
                DIG3 = 0;
                DIG4 = 0;
                break;
            case 2:
                DIG1 = 0;
                DIG2 = 0;
                DIG3 = 1;
                DIG4 = 0;
                break;
            case 3:
                DIG1 = 0;
                DIG2 = 0;
                DIG3 = 0;
                DIG4 = 1;
                break;     
        }
    }