#ifndef PROGRAM_H
#define	PROGRAM_H

#include <xc.h> 
#include "bus_058.h"

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */
    
    uint8_t * get_program_command( uint8_t address );
    
    void start_program_from_adress( uint8_t address );
    
    bool is_current_command_control();
    
    bool is_current_command_loop();
    
    bool is_current_command_jump();
    
    bool is_program_paused();
    
    void pause_program( bool state );
    
    void end_program();
    
    bool is_program_ended();
    
    void next_program_command();
    
    uint8_t get_command_relay_output();
    
    void perform_jump_to( uint8_t address );
    
    uint8_t get_loop_iterations();
    
    void next_loop_program_command();
    
    uint8_t get_loop_command_address();
    
    uint8_t get_jump_command_address();
    
    void read_program();
    
    void compile_and_set_command( uint8_t address, uint8_t command[4] );
    
    void erase_program();
    
    void save_program_to_eeprom();
    
    uint8_t get_command_minutes();
    
    uint8_t get_command_seconds();
    
#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */
