#include "timers.h"
#include <xc.h>

    void put_time_on_display( uint8_t minutes, uint8_t seconds ) {
        uint8_t digits[4];
        
        digits[0] = digit_to_symbol( minutes / 10 );
        digits[1] = digit_to_symbol( minutes % 10 );
        digits[2] = digit_to_symbol( seconds / 10 );
        digits[3] = digit_to_symbol( seconds % 10 );
        
        set_all_display_symbols( digits[0], digits[1], digits[2], digits[3] );
    }
    
    void init_timer0() {
        TMR0ON = 0;
        
        T08BIT = 1;
        T0CS = 0;
        PSA = 0;
        
        T0PS0 = 1;
        T0PS1 = 1;
        T0PS2 = 1;
        
        TMR0IF = 0;
        TMR0IE = 1;
        
        TMR0ON = 1;
    }

    void init_timers(void) {
       init_timer0();
    }   
   
    extern uint8_t program_state;
    
    uint8_t minutes = 0, seconds = 0;
    
    bool relays_output_configured = false;
    
    void perform_one_sec_timer(void) {
        //USARTWriteString("1sec");
       
        set_relay_output( ~get_relay_output() );
        
        commit_relay_output();
        /*
        if( get_relay_output() == 0 ) set_relay_output( 1 );
        else{
            set_relay_output( get_relay_output() << 1 );
        }
        
        commit_relay_output();
        */
        /*
        
        if( program_state == 5 ) { //reading program
            static bool half_shown = false;
            uint8_t com = get_program_command( address );
            //TODO if( *com == 0 ) empty;
            set_all_display_symbols( _OFF, _OFF, _OFF, _OFF );
            half_shown = !half_shown;
        }
        
        if( !( is_program_paused() )  ) return;
        if( is_program_ended() ) {
            //idle();
            return;
        }
        if( program_state == 3 ) {
            if( is_current_command_control() ) {
                if( seconds == 0) {
                    if( minutes == 0 ) {
                        relays_output_configured = false;
                        next_program_command(); 
                        minutes = get_command_minutes();
                        seconds = get_command_seconds();
                        return;                         
                    }
                    minutes--;
                    seconds = 60;
                }
                if( !relays_output_configured ) {
                    set_relay_output( get_command_relay_output() );
                    relays_output_configured = true;
                }
                put_time_on_display( minutes, seconds );                
            }else if( is_current_command_jump() ) {
                perform_jump_to( get_jump_command_address() );
            }else if( is_current_command_loop() ) {
                static uint8_t iteration = 0;
                if( iteration == get_loop_iterations() ) {
                    next_loop_program_command();
                    iteration = 0;
                    return;
                }
                iteration++;
                perform_jump_to( get_loop_command_address() );
            }
               
        }
*/
    }