
#include "bus_058.h"
#include "inputs.h"

    void init_inputs() {    
        INPUT1_TRIS = 1;
        INPUT2_TRIS = 1;
        INPUT3_TRIS = 1;
    }

    uint8_t get_jump_location( uint8_t mask ) {
        uint8_t inputs = ( INPUT3 << 3 ) | ( INPUT2 << 2 ) | ( INPUT1 << 1 );
        
        switch( mask ) {
            case 0: //3 2 1
                
                switch( inputs ) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                }
                
                break;
            case 1://3 2
                break;
            case 2://3 1
                break;
            case 3://3
                break;
            case 4://2 1
                break;
            case 5://2
                break;
            case 6://1
                break;
            case 7:
                return 0;
        };
        return 0;
        //return //10 20 30
    }