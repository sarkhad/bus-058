#ifndef TIMERS_H
#define	TIMERS_H

#include <xc.h>
#include "bus_058.h"
#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    void init_timers(void);
    
    void perform_one_sec_timer(void);

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

