
#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

#include <xc.h>
#include "bus_058.h"

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    void init_interrupts(void);
    
#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

