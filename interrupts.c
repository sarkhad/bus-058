

#include <xc.h>
#include "interrupts.h"

    void init_interrupts(void) {
        IPEN = 0;
        
        TMR0IF = 0;
        INT0IF = 0;
        RBIF = 0;
        
        TMR0IE = 1;
        INT0IE = 0;
        RBIE = 0;
        
        RBPU = 1;
        
        TMR0IP = 1;
        RBIP = 0;
        INT2IP = 0;
        INT1IP = 0;
        INT2IE = 0;
        INT1IE = 0;
        
        PSPIE = 0;
        ADIE = 0;
        
        RCIE = 1; //EUSART RX interrupt
        TXIE = 0;
        
        SSPIE = 0;
        CCP1IE = 0;
        TMR2IE = 0;
        TMR1IE = 0;
        
        OSCFIE = 0;
        CMIE = 0;
        EEIE = 0;
        BCLIE = 0;
        HLVDIE = 0;
        TMR3IE = 0;
        CCP2IE = 0;
        
        GIE = 1;
        PEIE = 0;
    }

    uint8_t tmr0_overflow_count = 0;
    
    //76
    void interrupt isr(void) {
        if( TMR0IE == 1 && TMR0IF == 1 ) {
            if( tmr0_overflow_count == 76 ) {
                perform_one_sec_timer();
                tmr0_overflow_count = 0;
            }else tmr0_overflow_count++;
            TMR0IF = 0;
        }
    }
    
/*
    void interrupt low_priority isr_low(void) {
    
    }

    void interrupt high_priority isr_high(void) {
    
    }
*/