
#include "program.h"

uint8_t program_counter = 1, previous_program_counter = 0, next_loop_program_counter = 0;
static uint8_t program[99][4];

uint8_t current_command[4];

static uint8_t program_paused = false;
static uint8_t program_ended = false;   

    uint8_t * get_program_command( uint8_t address ) {
        return program[address];
    }
    
    void start_program_from_adress( uint8_t address ) {
        program_counter = address;
        program_paused = false;
        program_ended = false;
        set_all_display_symbols( _OFF, _OFF, _OFF, _OFF );
        next_program_command();
    }
    
    void compile_and_set_command( uint8_t address, uint8_t command[7] ) {   
        uint8_t compiled_command[4] = {0,0,0,0}; 
                
        switch( command[0] ) {
            case 10:
                compiled_command[0] = (1<<7)|(1<<5);
                compiled_command[1] = ( command[1] << 4 )| command[2];
                compiled_command[2] = ( command[3] * 10 ) + command[4];
                compiled_command[3] = ( command[5] << 4 )| command[6];
                break;
            case 11:
                compiled_command[0] = ( 1<<6 ) | ( 1<<5 );  
                compiled_command[1] = ( command[1] << 4 ) | command[2];
                compiled_command[2] = ( command[3] * 10 ) + command[4];
                compiled_command[3] = ( command[5] << 4 ) | command[6];     
                break;
            default: 
                compiled_command[0] = (1<<7) | (1<<6);
                compiled_command[0] |= command[0];
                
                compiled_command[1] = (command[2] << 4) | (command[1]);
                
                compiled_command[2] = ( command[3] * 10 ) + command[4];
                compiled_command[3] = ( command[5] * 10 ) + command[6];
                break;
        }
        
        program[ address ][0] = compiled_command[0]; 
        program[ address ][1] = compiled_command[1];
        program[ address ][2] = compiled_command[2];
        program[ address ][3] = compiled_command[3];
    }
     
    bool is_current_command_control() {
        return !( current_command[0] & ( 1 << 5 ) ) ? true : false;
    }
    
    bool is_current_command_loop() {
        return !( current_command[0] & ( 1 << 7 ) ) ? true : false;
    }
    
    bool is_current_command_jump() {
        return !( current_command[0] & ( 1 << 6 ) ) ? true : false;
    }
    
    bool is_program_paused() {
        return program_paused;
    }
    
    bool is_program_ended() {
        return program_ended;
    }
    
    void next_program_command() {
        for( program_counter; program_counter < 100; program_counter++ ) {
            if( program[ program_counter ] != 0 ) {
                current_command[0] = program[ program_counter ][0];
                current_command[1] = program[ program_counter ][1];
                current_command[2] = program[ program_counter ][2];
                current_command[3] = program[ program_counter ][3];

                if( is_current_command_loop() ) {
                    next_loop_program_counter = program_counter++;
                }
                return;
            }
        }
        
        end_program();
        /* if control
         *  get_jump_location
         */        
    }
    
    void pause_program( bool state ) {
        program_paused = state;
    }
    
    void end_program() {
        program_ended = true;
        program_paused = false;
    }
    
    uint8_t get_command_relay_output() {
        return current_command[1];
    }
    
    void perform_jump_to( uint8_t address ) {
        program_counter = address;
        next_program_command();
    }
    
    uint8_t get_loop_iterations() {
        return current_command[3] >> 4;
    }
    
    void next_loop_program_command() {
        program_counter = next_loop_program_counter;
        next_program_command();
    }
    
    uint8_t get_loop_command_address() {
        return current_command[2];
    }
    
    uint8_t get_jump_command_address() {
        return current_command[2];
    }
    
    void read_program() {
    
    }
    
    void erase_program() {
        for( uint8_t i = 0; i <= 100; i++ ) {
            for( uint8_t y = 0; y < 4; y++ ) program[i][y] = 0;
        }
    }
    
    void save_program_to_eeprom() {
        
    }
    
    uint8_t get_command_minutes() {
        return current_command[2];
    }
    
    uint8_t get_command_seconds() {
        return current_command[3];
    }