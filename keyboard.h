#ifndef KEYBOARD_H
#define	KEYBOARD_H

#include <xc.h>
#include "bus_058.h"

#ifdef	__cplusplus
extern "C" {
#endif 
    
//-- Keyboard
#define ROW5 PORTBbits.RB4
#define ROW5_TRIS TRISB4
    
#define ROW4 PORTBbits.RB3
#define ROW4_TRIS TRISB3
    
#define ROW3 PORTBbits.RB2
#define ROW3_TRIS TRISB2
    
#define ROW2 PORTBbits.RB1
#define ROW2_TRIS TRISB1

#define ROW1 PORTBbits.RB0
#define ROW1_TRIS TRISB0

#define COL1 PORTDbits.RD4
#define COL1_TRIS TRISD4
   
#define COL2 PORTDbits.RD5
#define COL2_TRIS TRISD5

#define COL3 PORTDbits.RD6
#define COL3_TRIS TRISD6

#define COL4 PORTDbits.RD7
#define COL4_TRIS TRISD7
//--
    
/**
 * Prepare pins to work with keyboard
 */
void init_keyboard();
    
uint8_t get_first_button_pressed_code();

bool is_keyboard_locked();

void lock_keyboard( bool state );

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	// KEYBOARD.H