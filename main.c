/* TODO
 * 1. Keyboard debouncing and one time clicking. [ Implemented ] [Implemented v2]
 * 3. DS_OUT changed programmatically from RA4 to RB1. PCBV5 isn't equal. [PCBv6 fixed]
 * 4. PGM to RC5 
 */

#include "bus_058.h"

static void execute_button_action( uint8_t code );

static bool setup = true;

uint8_t program_state = 0, 
        previous_program_state = 0;

void init() {
    if( !setup ) return;

   //USARTInit();
   
   init_keyboard();
   init_relay_output();
   init_display();
   init_front_buttons();
   init_interrupts();
   
   init_timers();
   init_inputs();
   
   idle();
   
   //USARTWriteLine("Started"); // {com:"log",msg:"BUS-058 started."}
   
   setup = false;
}

void main(void) {
    init();
              
   // set_relay_output( 0xFF );
    
    while(1) {
        //uint8_t bid = get_first_button_pressed_code();
        //if( bid != 0 ) {
            //execute_button_action( bid );
            //USARTWriteInt(bid, 2); // {com:"log",msg:"Pressed " + bid + "."}
        //}
       // scan_front_buttons();
        show_display();
    }
    
    return;
}

/**
 * Program states:
 *  0 - idle. Only (A) buttons.
 *  1 - Waiting for command address. Only ( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ). Two-digit address. Format {'A', X, N, N}, where X is all segments off and N - digit 
 * -2 - Address typed. Waiting for command. Only ( P, 3n, 4t )
 * |- 3 - Performing program ( Ст, С )
 * |- |- 4 - Writing program
 *    |- 7  - Command typed. Only ( +1, A ) 
 * |- 5 - Reading program
 * 
 * 
 * 99 - typing hexadecimal digit
 *
 * Keyboard buttons IDs:
 * 1 - 7, 2 - 8, 3 - 9, 4 - П
 * 5 - 4, 6 - 5, 7 - 6, 8 - Ц
 * 9 - 1, 10 - 2, 11 - 3, 12 - СТ 
 * 13 - 0, 14 - А, 15 - ЗП, 16 - +1
 * 17 - P, 18 - С, 19 - F, 20 - Чт
 */

uint8_t address = 0;
static uint8_t addig = 0;

static uint8_t typed = 0;
static uint8_t typed_command[7];
static uint8_t type = 1;
/*
 * 1 - CONTROL
 * 2 - JUMP
 * 3 - LOOP
 */

static void execute_button_action( uint8_t button_code ) {
    if( is_keyboard_locked() ) return;
    
    switch( program_state ) {
        case 0: {
            if( button_code == 14 ) {
                set_all_display_symbols( _A, _OFF, _OFF, _OFF );
                program_state = 1;
                previous_program_state = 0;
            }         
            break;
        }
        case 1: { //rewrite this block
            static uint8_t mul = 1;
            uint8_t dig;
            
            switch( button_code ) {
                case 1: //7
                    address += 7 * mul;
                    dig = digit_to_symbol(7);
                    break;
                case 2: //8
                    address += 8 * mul;
                    dig = digit_to_symbol(8);
                    break;
                case 3: //9
                    address += 9 * mul;
                    dig = digit_to_symbol(9);
                    break;
                case 5: //4
                    address += 4 * mul;
                    dig = digit_to_symbol(4);
                    break;
                case 6: //5
                    address += 5 * mul;
                    dig = digit_to_symbol(5);
                    break;
                case 7: //6
                    address += 6 * mul;
                    dig = digit_to_symbol(6);
                    break;
                case 9: //1
                    address += 1 * mul;
                    dig = digit_to_symbol(1);
                    break;
                case 10: //2
                    address += 2 * mul;
                    dig = digit_to_symbol(2);
                    break;
                case 11://3
                    address += 3 * mul;
                    dig = digit_to_symbol(3);
                    break;
                case 13: //0
                    address += 0 * mul;
                    dig = digit_to_symbol(0);
                    break;
                default:
                    return;
            }
            
            shift_display_left( 1 );
            set_display_symbol( _A, 0 );
            set_display_symbol( dig, 3 );
            
            mul *= 10;
            if( mul == 100 ) {
                address =  ( ( address % 10 ) * 10 ) + ( address / 10 ); //delete it and rewrite fully that code block
                program_state = 2;
                previous_program_state = 1;
                mul = 1;
            }
            break;          
        }
        case 2: { //address typed
            switch( button_code ) {
                case 15: // ЗП
                    previous_program_state = 2;
                    program_state = 4;
                    set_all_display_symbols( _L, _OFF, _OFF, _OFF );
                    break;
                case 17: // P
                    previous_program_state = 2;
                    program_state = 3;
                    start_program_from_adress( address );
                    break;
                case 20: // Чт
                    previous_program_state = 2;
                    program_state = 5;
                    read_program();
                    break;
            }
            break;
        }
        case 3: { //performing program
            switch( button_code ) {
                case 12: //Ст Stop without outputs offing 
                    break;
                case 18: //C
                    break;
            }
            break;
        }
        case 4: { //writing program
//TODO type == 1 only up to octa 0..7 for CONTROL type
            uint8_t dig = 0;
 
            switch( button_code ) {
                case 1: //7
                    typed_command[typed] = 7;
                    dig = digit_to_symbol( 7 );
                    break;
                case 2: //8
                    if( typed == 0 ) return;
                    dig = digit_to_symbol( 8 );
                    typed_command[typed] = 8;
                    break;
                case 3: //9
                    if( typed == 0 ) return;
                    dig = digit_to_symbol( 9 );
                    typed_command[typed] = 9;
                    break;
                case 5: //4
                    dig = digit_to_symbol( 4 );
                    typed_command[typed] = 4;
                    break;
                case 6: //5
                    dig = digit_to_symbol( 5 );
                    typed_command[typed] = 5;
                    break;
                case 7: //6
                    dig = digit_to_symbol( 6 );
                    typed_command[typed] = 6;
                    break;
                case 9: //1
                    dig = digit_to_symbol( 1 );
                    typed_command[typed] = 1;
                    break;
                case 10: //2
                    dig = digit_to_symbol( 2 );
                    typed_command[typed] = 2;
                    break;
                case 11: //3
                    dig = digit_to_symbol( 3 );
                    typed_command[typed] = 3;
                    break;
                case 13: //0
                    dig = digit_to_symbol( 0 );
                    typed_command[typed] = 0;
                    break;
                case 4: //П 10
                    dig = _A;
                    
                    if( typed == 0 ) {
                        type = 2;
                        //return;
                    }
                    if( ( ( typed == 3 ) || ( typed == 4 ) ) && ( type == 2 ) ) {
                        return;
                    }
                    if( ( (typed == 0 ) || ( typed == 1 ) || ( typed == 2 ) || ( typed == 5 ) || (typed == 6 ) ) && type == 2 ) dig = _JUMP;
                    typed_command[typed] = 10;
                    break;
                case 8: //Ц 11
                    dig = _B;
                    if( typed == 0 ) {
                        type = 3;
                        //return;
                    }
                    if( ( ( typed == 3 ) || ( typed == 4 ) ) && ( type == 2 ) ) {
                        return;
                    }
                    
                    if( ( ( typed == 0 ) || ( typed == 1 ) || ( typed == 2 ) || ( typed == 6 ) ) && ( type == 3 ) ) dig = _LOOP;
                    
                    typed_command[typed] = 11;
                    break;
                case 19: //F
                    if( ( ( typed == 1 ) || ( typed == 2 ) ) && ( type == 1 ) ) {
                         if( _OFF != get_display_symbol(3) ) shift_display_left(1);
                        set_display_symbol( _OFF, 3 );
                        program_state = 99;
                        previous_program_state = 4;
                    }
                default: return;
            }

            if( _OFF != get_display_symbol(3) ) shift_display_left(1);
            set_display_symbol( _L, 0 );
            set_display_symbol( dig, 3 );
            
            typed++;
            if( typed == 7 ) {
                program_state = 7;
                previous_program_state = 4; 
                typed = 0;
            }
            break; 
        }
        case 5: {//reading program
                              /* USARTWriteString( "||" );
                    USARTWriteInt( address, 2 );
             uint8_t * com = get_program_command( address );
                    
                    USARTWriteInt( *(com), 3 );
                    USARTWriteString("|");
                    USARTWriteInt( *(com+1), 3 );
                    USARTWriteString("|");
                    USARTWriteInt( *(com+2), 3 );
                    USARTWriteString("|");
                   // USARTWriteInt( *(com+3), 3 );

                    com = get_program_command( address + 1 );
                    
                    //USARTWriteInt( *(com), 3 );
                    //USARTWriteString("|");
                    //USARTWriteInt( *(com+1), 3 );
                    //USARTWriteString("|");
                    //USARTWriteInt( *(com+2), 3 );
                    //USARTWriteString("|");
                    //USARTWriteInt( *(com+3), 3 );
            */
            switch( button_code ) {
                
                //get
                
                //default: return;
            }
            break;
        }
        case 7: {  // 7  - Command typed. Only ( +1, A )
            switch( button_code ) {
                case 14: {                   
                    //USARTWriteString( "||" );
                    //USARTWriteInt( address, 2 );
                    compile_and_set_command( address, typed_command );
                    
                    set_all_display_symbols( _A, _OFF, _OFF, _OFF );
                    
                    program_state = 1;
                    previous_program_state = 7;
                    address = 0;
                    break;
                }
                case 16: { //+1
                    compile_and_set_command( address, typed_command );
                    
                    if( address == 99 ) address = 1; else address++;
                                       
                    set_all_display_symbols( _L, _OFF, _OFF, _OFF );
                    
                    program_state = 4;
                    previous_program_state = 7;
                    break;
                }
                default: return;
            }
            break;
        }
        case 99: {            
            uint8_t hex_dig;
            switch( button_code ) {
                case 2: //8 12
                    typed_command[typed] = 12;
                    hex_dig = _C;
                    break;
                case 3: //9 13
                    typed_command[typed] = 13;
                    hex_dig = _D;
                    break;
                case 8: //Ц 14
                    typed_command[typed] = 14;
                    hex_dig = _E;
                    break;
                case 4: //П 15
                    typed_command[typed] = 15;
                    hex_dig = _F;
                    break;
                default: 
                    return;
            }
            
            set_display_symbol( _L, 0 );
            set_display_symbol(hex_dig, 3);
            shift_display_left(1);
            set_display_symbol( _OFF, 3 );
            
            typed++;
            program_state = previous_program_state;
            previous_program_state = 99;
        }
    }
}

    void idle() {
        program_state = 0;
        previous_program_state = 0;
        
        address = 0, 
        addig = 0;
        
        typed = 0;
        //typed_command[] = {0,0,0,0,0,0,0};
        typed_command[0] = 0;
        typed_command[1] = 0;
        typed_command[2] = 0;
        typed_command[3] = 0;
        typed_command[4] = 0;
        typed_command[5] = 0;
        typed_command[6] = 0;
        
        set_display_symbol( _I, 0 );
        set_display_symbol( _D, 1 );
        set_display_symbol( _L, 2 );
        set_display_symbol( _E, 3 );
        
        set_relay_output( ALL_RELAY_OUTPUTS_OFF );
    }
