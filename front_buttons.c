#include "front_buttons.h"
#include <xc.h>

bool lck_kb_pressed = true, rst_pressed = true, btr_pressed = true;
bool keyboard_locked = false;

    void init_front_buttons() {
        ADON = 0;
        ADCON1 = 0x0F; 
        CMCON = 0x07;
      
        LCK_KB_TRIS = 1;
        RST_TRIS = 1;
        BTR_TRIS = 1;
    }
    
    void scan_front_buttons() {        
        if( LCK_KB && !lck_kb_pressed ) {
            __delay_ms(5);
            if( LCK_KB ) {
                lock_keyboard_button_action();
            }
        }
        lck_kb_pressed = LCK_KB;
        
        if( RST && !rst_pressed ) {
            __delay_ms(5);
            if( RST ) {
                reset_button_action();
            }
        }
        rst_pressed = RST;
        
        if( BTR && !btr_pressed ) {
            __delay_ms(5);
            if( BTR ) {
                battery_button_action();
            }
        }
        btr_pressed = BTR;
    }

    void lock_keyboard_button_action() {
        lock_keyboard( !keyboard_locked );
        //USARTWriteString("Keyboard locked"); //{com:"log",msg:"Keyboard locked"}
    }
        
    void reset_button_action() {
                   //USARTWriteString("Reset performed1.");
        if( is_keyboard_locked() ) {
            idle();
            erase_program();
            //USARTWriteString("Reset performed.");
        }
        //erase program
        //turn off all outputs
        //is keyboard not locked
    }
        
    void battery_button_action() {
        //USARTWriteString("Program saved to EEPROM2");
        if( is_keyboard_locked() ) {
            save_program_to_eeprom();
            //USARTWriteString("Program saved to EEPROM");
        }
    }