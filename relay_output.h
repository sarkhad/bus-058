
#ifndef RELAY_OUTPUT_H
#define	RELAY_OUTPUT_H

#include <xc.h> 
#include "bus_058.h"

#ifdef	__cplusplus
extern "C" {
#endif 
    
//-- Relays control outputs
    #define SH_CP_OUT LATE2
    #define SH_CP_OUT_TRIS TRISE2
    
    #define DS_OUT LATE0
    #define DS_OUT_TRIS TRISE0
    
    #define ST_CP_OUT LATE1
    #define ST_CP_OUT_TRIS TRISE1
//--
    
    const uint8_t ALL_RELAY_OUTPUTS_OFF = 0;
    
    void init_relay_output();
    
    void set_relay_output( uint8_t output );
    
    uint8_t get_relay_output();
    
    void commit_relay_output();

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

