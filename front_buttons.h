
#ifndef FRONT_BUTTONS_H
#define	FRONT_BUTTONS_H

#include <xc.h>
#include "bus_058.h"

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

//-- Front panel buttons
#define LCK_KB PORTAbits.RA0
#define LCK_KB_TRIS TRISA0

#define RST PORTAbits.RA1
#define RST_TRIS TRISA1

#define BTR PORTAbits.RA2
#define BTR_TRIS TRISA2    
//--
    
    void init_front_buttons();
    
    void scan_front_buttons();
    
    void lock_keyboard_button_action();
    
    void reset_button_action();
    
    void battery_button_action();
    
    bool is_keyboard_locked();
    
#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

