
#include "keyboard.h"
    
uint8_t keymap[5][4] = {
    { 1, 2, 3, 4 },
    { 5, 6, 7, 8 },
    { 9, 10, 11, 12 },
    { 13, 14, 15, 16 },
    { 17, 18, 19,  20 }
};

bool kb_pressed_map[5][4] = {
    { true, true, true, true },
    { true, true, true, true },
    { true, true, true, true },
    { true, true, true, true },
    { true, true, true, true },
};   

void init_keyboard() {
    ROW5_TRIS = 1;
    ROW4_TRIS = 1;
    ROW3_TRIS = 1;
    ROW2_TRIS = 1;
    ROW1_TRIS = 1;

    COL1_TRIS = 1;
    COL2_TRIS = 1;
    COL3_TRIS = 1;
    COL4_TRIS = 1;
}

static bool keyboard_locked = false;

bool is_keyboard_locked() {
    return keyboard_locked;
}

void lock_keyboard( bool state ) {
    keyboard_locked = state;
}

static void pull_up_column( uint8_t column ) {
    switch( column ) {
        case 0:
            COL4_TRIS = 1;
            COL3_TRIS = 1;
            COL2_TRIS = 1;
            COL1 = 0;
            COL1_TRIS = 0;
            break;
        case 1:
            COL4_TRIS = 1;
            COL3_TRIS = 1;
            COL1_TRIS = 1;
            COL2 = 0;
            COL2_TRIS = 0;
            break;
        case 2:
            COL4_TRIS = 1;
            COL2_TRIS = 1;
            COL1_TRIS = 1;
            COL3 = 0;
            COL3_TRIS = 0;
            break;
        case 3:
            COL3_TRIS = 1;
            COL2_TRIS = 1;
            COL1_TRIS = 1;
            COL4 = 0;
            COL4_TRIS = 0;
            break;
        default:
            COL4_TRIS = 1;
            COL3_TRIS = 1;
            COL2_TRIS = 1;
            COL1_TRIS = 1;
    }
}

bool pressed = true;

/**
 * @return the first pressed button code 
 */
uint8_t get_first_button_pressed_code() {
    uint8_t code = 0;
          /*  if( LCK_KB && !lck_kb_pressed ) {
            __delay_ms(5);
            if( LCK_KB ) {
                lock_keyboard_button_action();
            }
        }*/
    for( uint8_t column = 0; column < 4; column++ ) {
        pull_up_column( column );    
 
        for( uint8_t row = 0; row < 5; row++ ) {
            switch( row ) {
                case 0:
                    /*
                    if( !ROW1 ) {  
                        while( !ROW1 );
                        code = keymap[0][column];
                    }*/
                    if( ROW1 && !(kb_pressed_map[0][column]) ) {
                        __delay_ms(5);
                        if( ROW1 ) {
                            code = keymap[0][column];
                        }
                    }
                    kb_pressed_map[0][column] = ROW1;
                    break;
                case 1:
                    /*if( !ROW2 ) {
                        while( !ROW2 );
                        code = keymap[1][column];
                    }*/
                    if( ROW2 && !(kb_pressed_map[1][column]) ) {
                        __delay_ms(5);
                        if( ROW2 ) {
                            code = keymap[1][column];
                        }
                    }
                    kb_pressed_map[1][column] = ROW2;
                    break;
                case 2:
                    /*if( !ROW3 ) {
                        while( !ROW3 );
                        code = keymap[2][column];
                    }*/
                    if( ROW3 && !(kb_pressed_map[2][column]) ) {
                        __delay_ms(5);
                        if( ROW3 ) {
                            code = keymap[2][column];
                        }
                    }
                    kb_pressed_map[2][column] = ROW3;
                    break;
                case 3:
                    /*if( !ROW4 ) {
                        while( !ROW4 );
                        code = keymap[3][column];
                    }*/
                    if( ROW4 && !(kb_pressed_map[3][column]) ) {
                        __delay_ms(5);
                        if( ROW4 ) {
                            code = keymap[3][column];
                        }
                    }
                    kb_pressed_map[3][column] = ROW4;
                    break;
                case 4:
                    /*if( !ROW5 ) {
                        while( !ROW5 );
                        code = keymap[4][column];
                    }*/
                    if( ROW5 && !(kb_pressed_map[4][column]) ) {
                        __delay_ms(5);
                        if( ROW5 ) {
                            code = keymap[4][column];
                        }
                    }
                    kb_pressed_map[4][column] = ROW5;
                    break;
            }
        }
    }
    
    pull_up_column(-1);
    return code;
}

