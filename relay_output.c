
#include "relay_output.h"

static uint8_t _output = 0x11;

void init_relay_output() {
    SH_CP_OUT = 0;
    SH_CP_OUT_TRIS = 0;

    DS_OUT = 0;
    DS_OUT_TRIS = 0;
    
    ST_CP_OUT = 0;
    ST_CP_OUT_TRIS = 0;
    
    commit_relay_output();
}

    void set_relay_output( uint8_t output ) {
      _output = output;
    }

    uint8_t get_relay_output() {
        return _output;
    }
    
    void commit_relay_output() {
        SH_CP_OUT = 0;
    
        for( uint8_t i = 0; i < 8; i++ ) {
            if( ( _output >> i ) & 1 ) DS_OUT = 1; else DS_OUT = 0;
            
            SH_CP_OUT = 1;
            __delay_us( 10 );
            SH_CP_OUT = 0;
        }
    
        ST_CP_OUT = 1;
        __delay_us( 10 );
        ST_CP_OUT = 0;
    }